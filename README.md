### What is this repository for? ###
QT based DICOM interface primarily intended for Voluson ultrasound device.

See wiki for more information [here](https://bitbucket.org/jirisliz/qtvolusoninterface/wiki/Home).