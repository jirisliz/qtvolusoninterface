#ifndef MYTEXTEDIT_H
#define MYTEXTEDIT_H

#include <QTextEdit>
#include <QKeyEvent>

/**
 * @brief Custom test edit - Ctrl+F shortcut integrated
 */
class myTextEdit : public QTextEdit
{
    Q_OBJECT
public:
    explicit myTextEdit(QWidget *parent = 0);

    void keyPressEvent(QKeyEvent *aKey);

signals:
    void findSignal();

public slots:
};

#endif // MYTEXTEDIT_H
