#include "dcmqfilesystemmodel.h"
#include "cvolusoninterface.h"

dcmQFileSystemModel::dcmQFileSystemModel()
{

}

QVariant dcmQFileSystemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    QVariant initial_value;
    initial_value = QFileSystemModel::data(index,role);

    // Too slow
//    if (role == Qt::DisplayRole)
//    {
//        QFileInfo info = dcmQFileSystemModel::fileInfo(index);
//        if(info.isFile())
//        {
//            if(initial_value.toString().contains(".dcm"))
//            {
//                // Change displayed filename using dicom tags

//                // Get patient name
//                QString name = cVolusonInterface::getDCMFileTag(info.absoluteFilePath(), "0010,0010");

//                // Number of frames
////                QString frames = cVolusonInterface::getDCMFileTag(info.absoluteFilePath(), "0028,0008");

//                // StudyDate
////                QString studyDate = cVolusonInterface::getDCMFileTag(info.absoluteFilePath(), "0008,0020");

////                QString newName = "Name: " + name + " Frames: " + frames + "Date: " + studyDate;
//                QString newName = "Name: " + name;
//                initial_value.setValue(newName);
//            }
//        }
//    }

    return initial_value;
}
