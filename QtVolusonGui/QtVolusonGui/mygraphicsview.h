#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsRectItem>

/**
 * @brief Custom graphic view - zooming and moving capabilities
 */
class mygraphicsview : public QGraphicsView
{
public:
    mygraphicsview(QWidget* parent = NULL);
protected:

    //Take over the interaction
    virtual void wheelEvent(QWheelEvent* event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);

private:
    QPoint _lastPos;
};

#endif // MYGRAPHICSVIEW_H
