#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mytreeview.h"
#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QFileDialog>
#include <QDebug>
#include <QErrorMessage>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QDirModel>

/**
 * @brief Constructor - Initialize global variables, creates Scene, Shows server selection window
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Create graphics scene
    mScene = new QGraphicsScene();
    ui->graphicsView->setScene(mScene);

    // Tell the user what just happened
    ui->statusBar->showMessage("App started.");

    mbError = false;
    mbCfgLoaded = false;
    mbServerStarded = false;
    mbMultiframeSelected = false;

    // Init find dialog as modal
    mFindDial = new dialogFind(this);
    mFindDial->setModal(true);

    // Create model for treeview
    mModel = new dcmQFileSystemModel();

    // Select server path
    selectServerPath();

    // Allocate instance of the class mVolInterf
    mVolInterf = new cVolusonInterface(mServerPath);

    // Set the initial sizes for QSplitter widgets
    refreshSpliter();

    QList<int> sizes;
    sizes << this->size().width()/3 << 2*this->size().width()/3;
    ui->splitter_2->setSizes(sizes);

    // init spinbox
    ui->sbFrame->setMinimum(1);
    ui->sbFrame->setMaximum(1);

    // init speed range FPS
    ui->sldSpeed->setMinimum(1);
    ui->sldSpeed->setMaximum(50);
    ui->sldSpeed->setValue(25);
    ui->lblSpeed->setText("25 FPS");

    // Init timer
    mTimerFrames = new QTimer(this);
    connect(mTimerFrames, SIGNAL(timeout()), this, SLOT(timerFrameUpdate()));

    ui->progressBar->setVisible(false);
    ui->widgetFrameControl->setVisible(false);

    // init pointer to null
    mLoadImgThread = Q_NULLPTR;

    // Connect textEdit find signal
    connect(ui->textEdit, SIGNAL(findSignal()), this, SLOT(on_actionFind_text_triggered()));
    ui->textEdit->moveCursor(QTextCursor::Start);

    ui->widget->setVisible(false);

    QIcon icon(":icons/dicomIco.png");
    this->setWindowIcon(icon);

    this->setWindowState(Qt::WindowMaximized);

    // Start DICOM button animation
    aEffect= new QGraphicsColorizeEffect(ui->btnStartServer);
    ui->btnStartServer->setGraphicsEffect(aEffect);

    mPAnimation = new QPropertyAnimation(aEffect,"color");
    mPAnimation->setStartValue(QColor(Qt::black));
    mPAnimation->setKeyValueAt(0.5, QColor(Qt::red));
    mPAnimation->setEndValue(QColor(Qt::black));
    mPAnimation->setDuration(1000);
    mPAnimation->setLoopCount(-1);

    if(mbCfgLoaded)mPAnimation->start();

}

/**
 * @brief Destructor
 */
MainWindow::~MainWindow()
{
    // Delete if not null
    if(mLoadImgThread != Q_NULLPTR)
    {
        if(mLoadImgThread->isRunning())
        {
            mLoadImgThread->terminate();
            mLoadImgThread->wait();
        }

        delete mLoadImgThread;
    }

    delete mScene;
    delete mModel;
    delete mVolInterf;
    delete ui;
}

/**
 * @brief Starts thread for loading image frames
 * @param aFrames number of the fremes
 * @param aFramePath output frames path
 * @param aSelPath input dcm file path
 */
void MainWindow::newLoadImgThread(unsigned int aFrames, QString aFramePath, QString aSelPath)
{
    // Delete if not null
    if(mLoadImgThread != Q_NULLPTR)
    {
        if(mLoadImgThread->isRunning())
        {
            mLoadImgThread->terminate();
            mLoadImgThread->wait();
        }

        delete mLoadImgThread;
    }

    // Create new thread for load image frames
    mLoadImgThread = new loadImgsThread(aFrames, aFramePath, aSelPath, this);

    // Connect
    connect(mLoadImgThread, SIGNAL(numChanged(int)), this, SLOT(loadImgChanged(int)));
    connect(mLoadImgThread, SIGNAL(ended()), this, SLOT(loadImgEnded()));

    // Start thread
    mLoadImgThread->start();
}

/**
 * @brief Refreshes splitter
 */
void MainWindow::refreshSpliter()
{
    QList<int> sizesLst = ui->splitter->sizes();
    int h = ui->splitter->height();
    sizesLst[0] = h/3;
    sizesLst[1] = h/3;
    sizesLst[2] = h/3;

    ui->splitter->setSizes(sizesLst);
}

/**
 * @brief Shows find dialog which search text in textEdit
 */
void MainWindow::findText()
{
    ui->menuBar->setEnabled(false);
    if(mFindDial->exec() == QDialog::Accepted)
    {
        bool res = ui->textEdit->find(mFindDial->mSearchStr);
        if(!res)
        {
            QTextCursor cursor = ui->textEdit->textCursor();
            cursor.movePosition(QTextCursor::Start);
            ui->textEdit->setTextCursor(cursor);
            ui->textEdit->find(mFindDial->mSearchStr);
        }
        mTextCursor = ui->textEdit->textCursor();
    }
    ui->menuBar->setEnabled(true);
}

/**
 * @brief Selects server path by loading cfg file, loads dcm files to treeView
 */
void MainWindow::selectServerPath()
{
    // Open file dialog
    QString fileName = QFileDialog::getOpenFileName(this,
             tr("Open server dcmqrscp.cfg file"), QDir::currentPath(), tr("CFG File (*.cfg)"));
    QFile f(fileName);

    QString path = f.fileName();
    QString dir = path.section("/",0,-2);

    // Check presence of the dcmqrscp.cfg file
    if(!f.exists())
    {
        mbError = true;
        QMessageBox msgBox;
        msgBox.setText("Server file dcmqrscp.cfg not found. Use <b>File->Open server cfg file</b> to define server location.");
        msgBox.setWindowTitle("Error!");
        msgBox.exec();

        ui->statusBar->showMessage("Use \"File->Open server cfg file\" to define server location.");
        return;
    }

    mServerPath = dir;
    mModel->setRootPath(mServerPath);

    // Set filter
    QStringList fileFilter;
    fileFilter << "*.dcm";
    mModel->setNameFilters(fileFilter);
    mModel->setNameFilterDisables(false);
    mModel->setReadOnly(true);

    // Bind model to treeview
    ui->treeView->setModel(mModel);

    // Enable sorting
    ui->treeView->setSortingEnabled(true);

    // Set root folder ()
    ui->treeView->setRootIndex(mModel->index(mServerPath));

    // Connect keypress slot with treeView signal
    myTreeView *treeView = ui->treeView;
    connect(treeView, SIGNAL(keyPressed()),this,SLOT(treeViewKeyPressed()));
    treeView->header()->setStretchLastSection(false);
    treeView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

    ui->btnStartServer->setEnabled(true);
    ui->btnDownload->setEnabled(true);

    // Tell the user what just happened
    ui->statusBar->showMessage("Server path selected. You can now start the DICOM server.");

    mbCfgLoaded = true;
}

/**
 * @brief Shows info about dcm file in textView and also preview in selected graphical out
 * @param aDcmPath dcm file path
 */
void MainWindow::showDcmInfo(const QString aDcmPath)
{
    // If no change return
    if(aDcmPath == mSelectedPath)
    {
        return;
    }

    // Check if the file is dicom
    if(!(aDcmPath.endsWith(".dcm") || aDcmPath.endsWith(".DCM")))
    {
        if(!ui->actionVTK->isChecked())
            return;
    }

    ui->textEdit->setText(mVolInterf->getDCMFileTags(aDcmPath));

    mSelectedPath = aDcmPath;

    if(mTimerFrames->isActive())
    {
        ui->btnPlay->setText("Start");
        mTimerFrames->stop();
    }

    ui->btnPlay->setEnabled(false);
    ui->sbFrame->setEnabled(false);
    ui->sldSpeed->setEnabled(false);
    ui->progressBar->setVisible(false);
    ui->widgetFrameControl->setVisible(false);

    if(ui->actionQGraphicView->isChecked())
    {
        showDCMImageQT(mSelectedPath);
    }

    if(ui->actionVTK->isChecked())
    {
        showDCMImageVTK(mSelectedPath);
    }
}

/**
 * @brief QT baset dcm image viewer
 * @param aDcmPath dcm file path
 */
void MainWindow::showDCMImageQT(QString aDcmPath)
{
    // Clear scene
    mScene->clear();

    // Get frames number
    QString strFrameNum = mVolInterf->getDCMFileTag(aDcmPath, DICOM_TAG_NumberOfFrames);
    int frameNum = strFrameNum.toInt();
    ui->sbFrame->setMaximum(frameNum);
    ui->sbFrame->setValue(1);
    QImage image;

    // Convert dcm file to jpg
    QString jpgPath = mVolInterf->convertDcmToImg(aDcmPath, aDcmPath.section("/",0,-2), 1);
    // Open and show jpg file
    image = QImage(jpgPath);

    if(frameNum)
    {
        QGraphicsPixmapItem* item = new QGraphicsPixmapItem(QPixmap::fromImage(image));
        mScene->addItem(item);

        // scale image to view size
        ui->graphicsView->fitInView(mScene->itemsBoundingRect(), Qt::KeepAspectRatio);

        mbMultiframeSelected = true;

        // Load all frames
        loadFrames();
    }
    else
    {
        mbMultiframeSelected = false;

        if(image.isNull())
        {
            QGraphicsTextItem *text = mScene->addText("No image in the selected dcm file");
            text->setPos(100, 200);
            mScene->addItem(text);

            // scale image to view size
            ui->graphicsView->fitInView(mScene->itemsBoundingRect(), Qt::KeepAspectRatio);
            return;
        }
        QGraphicsPixmapItem* item = new QGraphicsPixmapItem(QPixmap::fromImage(image));
        mScene->addItem(item);

        // scale image to view size
        ui->graphicsView->fitInView(mScene->itemsBoundingRect(), Qt::KeepAspectRatio);
    }
}

/**
 * @brief VTK baset dcm image viewer
 * @param aDcmPath dcm file path
 */
void MainWindow::showDCMImageVTK(QString aDcmPath)
{
    // Decompress dcm file if needed
//    DICOM_TAG_LossyImageCompression
    QString strCompress = mVolInterf->getDCMFileTag(aDcmPath, DICOM_TAG_LossyImageCompression);
    QString decPath = aDcmPath;
    if(strCompress.contains("01"))
    {
        QString framesPath = aDcmPath.section("/",0,-2) + "/" + FRAMES_FOLDER_NAME;
        decPath = mVolInterf->decompressJpegDcm(aDcmPath, framesPath);
    }

    // Show decompressed dcm file
    ui->widget->draw(decPath);
}

/**
 * @brief Show single frame - used for animation of multiframe images
 * @param aFrame number of frame
 */
void MainWindow::showFrame(int aFrame)
{
    if(aFrame < 1)return;

    // Clear graphics scene
    mScene->clear();


    // Open and show jpg (pnm) file
    QImage image(mSelectedPath.section("/",0,-2) + "/" + FRAMES_FOLDER_NAME + "/image" + QString::number(aFrame));

    QGraphicsPixmapItem* item = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    mScene->addItem(item);

    // scale image to view size
    //ui->graphicsView->fitInView(mScene->itemsBoundingRect(), Qt::KeepAspectRatio);
}

/**
 * @brief Button btnStartServer clicked slot
 */
void MainWindow::on_btnStartServer_clicked()
{
    if(!mbServerStarded)
    {
        // Start the DICOM serfer thread
        mVolInterf->startServer();

        // Check server with delay 500ms
        QTimer::singleShot(500, this, SLOT(updateServerInfo()));
    }
    else
    {
        // Stop the DICOM server
        mVolInterf->stopServer();

        // Check server with delay 500ms
        QTimer::singleShot(500, this, SLOT(updateServerInfo()));
    }
}

/**
 * @brief Update of the server info status bar
 */
void MainWindow::updateServerInfo()
{
    if(mVolInterf->isServerStarted())
    {
        ui->statusBar->showMessage("DICOM server is running.\n");
        ui->btnStartServer->setText("Stop DICOM server");
        ui->btnStartServer->setToolTip("Stop DICOM server.");

        aEffect->setColor(QColor(Qt::black));
        //ui->btnStartServer->setGraphicsEffect(eEffect);

        // Enable buttons that needs dcmtk server running
        ui->btnFind->setEnabled(true);
        mbServerStarded = true;
        mPAnimation->stop();
    }
    else
    {
        ui->statusBar->showMessage("DICOM server is stopped.\n");
        ui->btnStartServer->setText("Start DICOM server");
        ui->btnStartServer->setToolTip("Start DICOM server to estabilish communication.");
        ui->btnFind->setEnabled(false);
        mbServerStarded = false;
        mPAnimation->start();
    }
}

/**
 * @brief Button btnFind clicked slot
 */
void MainWindow::on_btnFind_clicked()
{
    ui->textEdit->append("\n-------------------- FIND STUDIES --------------------\n");
    ui->textEdit->append(mVolInterf->findStudies());
}

/**
 * @brief Button btnDownload clicked
 */
void MainWindow::on_btnDownload_clicked()
{
    // Get selectedfile path
    QModelIndexList lst = ui->treeView->selectionModel()->selectedIndexes();
    if(lst.size() == 0)
    {
        // Tell the user what just happened
        ui->statusBar->showMessage("No item selected to download.", 5000);
        return;
    }
    QModelIndex index = lst[0];
    QString dcmPath = mModel->filePath(index);

    if(!(dcmPath.endsWith(".dcm") || dcmPath.endsWith(".DCM")))
    {
        // Tell the user what just happened
        ui->statusBar->showMessage("Please select *.dcm file to download.", 5000);
        return;
    }

    // Save file dialog
    QString saveFileName = QFileDialog::getSaveFileName(0, "Save file", "untitled", ".dcm");

    if(saveFileName == "")return;

    if (!(saveFileName.endsWith(".dcm") || saveFileName.endsWith(".DCM")))
        saveFileName += ".dcm";

    QFile file(dcmPath);
    file.copy(saveFileName);

}

/**
 * @brief Button btnPlay clicked
 */
void MainWindow::on_btnPlay_clicked()
{
    if(mTimerFrames->isActive())
    {
        ui->btnPlay->setText("Start");
        mTimerFrames->stop();
    }
    else
    {
        ui->btnPlay->setText("Stop");
        startTimer();
    }
}

/**
 * @brief Handle close event - show warning before close
 * @param event
 */
void MainWindow::closeEvent(QCloseEvent *event)
{
    // Ask if user really wants to close the app

    if(!mbError && mbServerStarded)
    {
        event->ignore();
        if (QMessageBox::Yes == QMessageBox::question(this, "Close Confirmation",
                            "Really want to exit?\nEvery running store/move processes will be stopped!",
                            QMessageBox::Yes | QMessageBox::No))
        {
            event->accept();
        }
    }
    else
    {
        event->accept();
    }
}

/**
 * @brief treeView clicked slot
 * @param index current selection
 */
void MainWindow::on_treeView_clicked(const QModelIndex &index)
{
    QString file = mModel->filePath(index);
    showDcmInfo(file);
}

/**
 * @brief treeView KeyPressed
 */
void MainWindow::treeViewKeyPressed()
{
    QModelIndexList lst = ui->treeView->selectionModel()->selectedIndexes();
    QModelIndex index = lst[0];
    QString file = mModel->filePath(index);
    showDcmInfo(file);
}

/**
 * @brief btnClear clicked
 */
void MainWindow::on_btnClear_clicked()
{
    ui->textEdit->clear();
}

/**
 * @brief btnFind_2 clicked
 */
void MainWindow::on_btnFind_2_clicked()
{
    findText();
}

/**
 * @brief splitter - splitterMoved
 * @param pos
 * @param index
 */
void MainWindow::on_splitter_splitterMoved(int pos, int index)
{
    // scale image to view size
    ui->graphicsView->fitInView(ui->graphicsView->scene()->itemsBoundingRect(), Qt::KeepAspectRatio);
}

/**
 * @brief splitter_2 - splitterMoved
 * @param pos
 * @param index
 */
void MainWindow::on_splitter_2_splitterMoved(int pos, int index)
{
    // scale image to view size
    ui->graphicsView->fitInView(ui->graphicsView->scene()->itemsBoundingRect(), Qt::KeepAspectRatio);
}



/**
 * @brief Spin box sbFrame valueChanged
 * @param arg1 new value
 */
void MainWindow::on_sbFrame_valueChanged(int arg1)
{
    showFrame(arg1);
}

/**
 * @brief timerFrame Update
 */
void MainWindow::timerFrameUpdate()
{
    // Increase frame and show
    int val = ui->sbFrame->value();
    val++;
    if(val >= ui->sbFrame->maximum())val = 1;
    ui->sbFrame->setValue(val);
    showFrame(val);
}

/**
 * @brief load Frames - starts separate thread for loading image frames
 */
void MainWindow::loadFrames()
{
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum(ui->sbFrame->maximum());
    ui->progressBar->setVisible(true);

    if(mLoadImgThread != Q_NULLPTR)
    {
        mLoadImgThread->stop();
        mLoadImgThread->terminate();
        mLoadImgThread->wait();
    }

    // Load all images to frames folder to enable animation
    QString framesPath = mSelectedPath.section("/",0,-2) + "/" + FRAMES_FOLDER_NAME;
    QDir dir(framesPath);
    if(dir.exists())
        dir.rmpath(framesPath);
    QDir().mkpath(framesPath);

    // Start load image thread
    newLoadImgThread(ui->sbFrame->maximum(), framesPath, mSelectedPath);
}

/**
 * @brief Slot - Update progress bar for loading images - connected with thread
 * @param aNum new value
 */
void MainWindow::loadImgChanged(int aNum)
{
     ui->progressBar->setValue(aNum);
}

/**
 * @brief Slot - Called when load images thread finishes
 */
void MainWindow::loadImgEnded()
{
    if(mbMultiframeSelected)
    {
        ui->progressBar->setVisible(false);
        ui->widgetFrameControl->setVisible(true);
        ui->btnPlay->setEnabled(true);
        ui->sbFrame->setEnabled(true);
        ui->sldSpeed->setEnabled(true);
    }
    else
    {
        ui->progressBar->setVisible(false);
        ui->widgetFrameControl->setVisible(false);
    }

    // scale image to view size
    ui->graphicsView->fitInView(mScene->itemsBoundingRect(), Qt::KeepAspectRatio);
}

/**
 * @brief Start timer for animation of multiframe images
 */
void MainWindow::startTimer()
{
    mTimerFrames->stop();
    float val = (float)ui->sldSpeed->value();
    int ms = (1/val)*1000;
    mTimerFrames->start(ms);
}

/**
 * @brief Slider sldSpeed valueChanged
 * @param value new value
 */
void MainWindow::on_sldSpeed_valueChanged(int value)
{
    ui->lblSpeed->setText(QString::number(value) + " FPS");
}

/**
 * @brief Slider sldSpeed sliderReleased
 */
void MainWindow::on_sldSpeed_sliderReleased()
{
    if(mTimerFrames->isActive())
    {
        startTimer();
    }
}

/**
 * @brief Menu item actionOpen_cfg triggered
 */
void MainWindow::on_actionOpen_cfg_triggered()
{
    // Select server path
    selectServerPath();
}

/**
 * @brief Menu item actionExit triggered
 */
void MainWindow::on_actionExit_triggered()
{
    this->close();
}

/**
 * @brief Menu item actionOpen_DICOM_file triggered
 */
void MainWindow::on_actionOpen_DICOM_file_triggered()
{
    // Open single DICOM file
    QString filter = "DICOM file (*.dcm)";
    QString file = QFileDialog::getOpenFileName(this, "Select a file...", QDir::currentPath(), filter);
    ui->textEdit->setText(mVolInterf->getDCMFileTags(file));

    // TODO: call showDcmInfo after changing its input param to QString
    showDcmInfo(file);
}

/**
 * @brief Menu item actionCreate_server_folder triggered
 */
void MainWindow::on_actionCreate_server_folder_triggered()
{
    // Open folder
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open/Create DICOM server directory"),
                                                             "/home",
                                                             QFileDialog::ShowDirsOnly
                                                             | QFileDialog::DontResolveSymlinks);

    mVolInterf->createServerFolder(dir);
}

/**
 * @brief Menu item actionFind_text triggered
 */
void MainWindow::on_actionFind_text_triggered()
{
    findText();
}

/**
 * @brief Menu item actionQGraphicView triggered
 */
void MainWindow::on_actionQGraphicView_triggered()
{
    ui->actionQGraphicView->setChecked(true);
    ui->actionVTK->setChecked(false);

    ui->widget->setVisible(false);
    ui->verticalLayout_5->setVisible(true);

    refreshSpliter();
}

/**
 * @brief Menu item actionVTK triggered
 */
void MainWindow::on_actionVTK_triggered()
{
    ui->actionQGraphicView->setChecked(false);
    ui->actionVTK->setChecked(true);

    ui->widget->setVisible(true);
    ui->verticalLayout_5->setVisible(false);

    ui->widget->draw(mSelectedPath);

    refreshSpliter();
}

