#ifndef MYTREEVIEW_H
#define MYTREEVIEW_H
#include <QTreeView>

/**
 * @brief Custom TreeView class - keypress event handled
 */
class myTreeView : public QTreeView
{
    Q_OBJECT

public:
    explicit myTreeView(QObject *parent = 0);

    void keyPressEvent(QKeyEvent *aKey);

signals:
    void keyPressed();

public slots:
};

#endif // MYTREEVIEW_H
