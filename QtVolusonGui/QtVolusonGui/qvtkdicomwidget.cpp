#include <QDebug>
#include <QWheelEvent>
#include "qvtkdicomwidget.h"

vtkStandardNewMacro(myVtkInteractorStyleImage);

QVtkDICOMWidget::QVtkDICOMWidget(QWidget *parent) : QVTKWidget(parent)
{
    // Init
    readerDCMSeries = vtkSmartPointer<vtkDICOMImageReader>::New();
    imageViewerDCMSeriesX = vtkSmartPointer<vtkImageViewer2>::New();
    sliceTextMapper = vtkSmartPointer<vtkTextMapper>::New();

    mSlice = 0;
    mMinSliderX = 0;
    mMaxSliderX = 0;
}

void QVtkDICOMWidget::draw(QString aDcmPath)
{
    std::string folder = aDcmPath.toStdString();

    if((aDcmPath.endsWith(".dcm") || aDcmPath.endsWith(".DCM")))
    {
        readerDCMSeries->SetFileName(folder.c_str());
    }
    else
    {
        readerDCMSeries->SetDirectoryName(folder.c_str());
    }

    readerDCMSeries->Update();

    // Visualize
    imageViewerDCMSeriesX->SetInputConnection(readerDCMSeries->GetOutputPort());

    // slice status message
    vtkSmartPointer<vtkTextProperty> sliceTextProp = vtkSmartPointer<vtkTextProperty>::New();
    sliceTextProp->SetFontFamilyToCourier();
    sliceTextProp->SetFontSize(20);
    sliceTextProp->SetVerticalJustificationToBottom();
    sliceTextProp->SetJustificationToLeft();

    std::string msg = StatusMessage::Format(imageViewerDCMSeriesX->GetSliceMin(),
                                            imageViewerDCMSeriesX->GetSliceMax());
    sliceTextMapper->SetInput(msg.c_str());
    sliceTextMapper->SetTextProperty(sliceTextProp);

    vtkSmartPointer<vtkActor2D> sliceTextActor = vtkSmartPointer<vtkActor2D>::New();
    sliceTextActor->SetMapper(sliceTextMapper);
    sliceTextActor->SetPosition(15, 10);

    // usage hint message
    vtkSmartPointer<vtkTextProperty> usageTextProp = vtkSmartPointer<vtkTextProperty>::New();
    usageTextProp->SetFontFamilyToCourier();
    usageTextProp->SetFontSize(14);
    usageTextProp->SetVerticalJustificationToTop();
    usageTextProp->SetJustificationToLeft();

    vtkSmartPointer<vtkTextMapper> usageTextMapper = vtkSmartPointer<vtkTextMapper>::New();
    usageTextMapper->SetInput("- Slice with mouse wheel\n  or Up/Down-Key\n- Zoom with pressed right\n  mouse button while dragging");
    usageTextMapper->SetTextProperty(usageTextProp);

    vtkSmartPointer<vtkActor2D> usageTextActor = vtkSmartPointer<vtkActor2D>::New();
    usageTextActor->SetMapper(usageTextMapper);
    usageTextActor->GetPositionCoordinate()->SetCoordinateSystemToNormalizedDisplay();
    usageTextActor->GetPositionCoordinate()->SetValue( 0.05, 0.95);

    // create an interactor with our own style (inherit from vtkInteractorStyleImage)
    // in order to catch mousewheel and key events
    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
            this->GetRenderWindow()->GetInteractor();

    vtkSmartPointer<myVtkInteractorStyleImage> myInteractorStyle =
            vtkSmartPointer<myVtkInteractorStyleImage>::New();

    // make imageviewer2 and sliceTextMapper visible to our interactorstyle
    // to enable slice status message updates when scrolling through the slices
    myInteractorStyle->SetImageViewer(imageViewerDCMSeriesX);
    myInteractorStyle->SetStatusMapper(sliceTextMapper);

    imageViewerDCMSeriesX->SetupInteractor(renderWindowInteractor);
    // make the interactor use our own interactorstyle
    // cause SetupInteractor() is defining it's own default interatorstyle
    // this must be called after SetupInteractor()
    renderWindowInteractor->SetInteractorStyle(myInteractorStyle);
    // add slice status message and usage hint message to the renderer
    imageViewerDCMSeriesX->GetRenderer()->AddActor2D(sliceTextActor);
    imageViewerDCMSeriesX->GetRenderer()->AddActor2D(usageTextActor);

    mMinSliderX = imageViewerDCMSeriesX->GetSliceMin();
    mMaxSliderX = imageViewerDCMSeriesX->GetSliceMax();

    imageViewerDCMSeriesX->SetRenderWindow(this->GetRenderWindow());

    imageViewerDCMSeriesX->GetRenderer()->ResetCamera();
    imageViewerDCMSeriesX->Render();
}

void QVtkDICOMWidget::moveSlice(int aDir)
{
    if(aDir > 0)
    {
        mSlice += 1;
        if(mSlice > mMaxSliderX)mSlice = mMaxSliderX;
        cout << "MoveSliceForward::Slice = " << mSlice << std::endl;
    }
    if(aDir < 0)
    {
        mSlice -= 1;
        if(mSlice < mMinSliderX)mSlice = mMinSliderX;
        cout << "MoveSliceBackward::Slice = " << mSlice << std::endl;
    }
    imageViewerDCMSeriesX->SetSlice(mSlice);
    std::string msg = StatusMessage::Format(mSlice, mMaxSliderX);
    sliceTextMapper->SetInput(msg.c_str());
    imageViewerDCMSeriesX->Render();
}



void QVtkDICOMWidget::wheelEvent(QWheelEvent *e)
{
    //    qDebug() << QString::number(e->delta());
    moveSlice(e->delta());
}

void QVtkDICOMWidget::keyPressEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Up)
    {
        moveSlice(1);
        return;
    }
    if(e->key() == Qt::Key_Down)
    {
        moveSlice(-1);
        return;
    }
    e->ignore();
}
