#-------------------------------------------------
#
# Project created by QtCreator 2016-10-27T12:12:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtVolusonGui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cvolusoninterface.cpp \
    mytreeview.cpp \
    mygraphicsview.cpp \
    dcmqfilesystemmodel.cpp \
    loadimgsthread.cpp \
    dialogfind.cpp \
    mytextedit.cpp \
    qvtkdicomwidget.cpp

HEADERS  += mainwindow.h \
    cvolusoninterface.h \
    mytreeview.h \
    mygraphicsview.h \
    dcmqfilesystemmodel.h \
    loadimgsthread.h \
    dialogfind.h \
    mytextedit.h \
    qvtkdicomwidget.h

FORMS    += mainwindow.ui \
    dialogfind.ui

RESOURCES += \
    res.qrc
