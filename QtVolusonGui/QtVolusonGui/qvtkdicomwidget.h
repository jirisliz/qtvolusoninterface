#ifndef QVTKDICOMWIDGET_H
#define QVTKDICOMWIDGET_H

#include <vtkSmartPointer.h>
#include <vtkObjectFactory.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
// headers needed for this example
#include <vtkImageViewer2.h>
#include <vtkDICOMImageReader.h>
#include <vtkInteractorStyleImage.h>
#include <vtkActor2D.h>
#include <vtkTextProperty.h>
#include <vtkTextMapper.h>
#include <QVTKWidget.h>
// needed to easily convert int to std::string
#include <sstream>

#include <QtCore>

#include <QWidget>


/**
 * @brief Helper class to format slice status message
 */
class StatusMessage {
public:
   static std::string Format(int slice, int maxSlice) {
      std::stringstream tmp;
      tmp << "Slice Number  " << slice + 1 << "/" << maxSlice + 1;
      return tmp.str();
   }
};

/**
 * @brief VTK style class
 */
class myVtkInteractorStyleImage : public vtkInteractorStyleImage
{
public:
   static myVtkInteractorStyleImage* New();
   vtkTypeMacro(myVtkInteractorStyleImage, vtkInteractorStyleImage);

protected:
   vtkImageViewer2* _ImageViewer;
   vtkTextMapper* _StatusMapper;
   int _Slice;
   int _MinSlice;
   int _MaxSlice;

public:
   void SetImageViewer(vtkImageViewer2* imageViewer) {
      _ImageViewer = imageViewer;
      _MinSlice = imageViewer->GetSliceMin();
      _MaxSlice = imageViewer->GetSliceMax();
      _Slice = _MinSlice;
      cout << "Slicer: Min = " << _MinSlice << ", Max = " << _MaxSlice << std::endl;
   }

   void SetStatusMapper(vtkTextMapper* statusMapper) {
      _StatusMapper = statusMapper;
   }
};

/**
 * @brief Custom QVtkWidget class for DICOM files
 */
class QVtkDICOMWidget : public QVTKWidget
{
    Q_OBJECT
public:
    explicit QVtkDICOMWidget(QWidget *parent = 0);

    void draw(QString aDcmPath);


private:

    vtkSmartPointer<vtkDICOMImageReader> readerDCMSeries;

    vtkSmartPointer<vtkImageViewer2> imageViewerDCMSeriesX;

    vtkSmartPointer<vtkTextMapper> sliceTextMapper;

    int mMinSliderX;

    int mMaxSliderX;

    int mSlice;

    void moveSlice(int aDir);

signals:

protected:
    virtual void wheelEvent(QWheelEvent *e);
    virtual void keyPressEvent(QKeyEvent *e);


};

#endif // QVTKDICOMWIDGET_H
