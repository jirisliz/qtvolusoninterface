#ifndef LOADIMGSTHREAD_H
#define LOADIMGSTHREAD_H

#include <QThread>

/**
 * @brief Thread for loading image frames in multiframe dcm files
 */
class loadImgsThread : public QThread
{
    Q_OBJECT
public:
    explicit loadImgsThread(QObject *parent = 0);
    loadImgsThread(unsigned int aFrames, QString aFramePath, QString aSelPath, QObject *parent = 0);
    ~loadImgsThread();
    void run();
    void stop();

signals:
    void numChanged(int);
    void ended();

public slots:

private:
    bool mStop;

    unsigned int mFrames;
    QString mFramePath;
    QString mSelPath;
};

#endif // LOADIMGSTHREAD_H
