#include "loadimgsthread.h"
#include "cvolusoninterface.h"
#include <QtCore>

loadImgsThread::loadImgsThread(QObject *parent) : QThread(parent)
{
    mStop = false;
}

loadImgsThread::loadImgsThread(unsigned int aFrames, QString aFramePath, QString aSelPath, QObject *parent)
 : QThread(parent)
{
    mStop = false;

    mFrames = aFrames;
    mFramePath = aFramePath;
    mSelPath = aSelPath;
}

loadImgsThread::~loadImgsThread()
{
    mStop = true;
}

void loadImgsThread::run()
{
    for(int i = 1 ; i <= mFrames ; i++)
    {
        cVolusonInterface::convertDcmToImg(mSelPath, mFramePath, i);
        QMutex mutex;
        mutex.lock();
        if(mStop)return;
        mutex.unlock();

        emit numChanged(i);
    }
    emit ended();
}

void loadImgsThread::stop()
{
    QMutex mutex;
    mutex.lock();
    mStop = true;
    mutex.unlock();
}
