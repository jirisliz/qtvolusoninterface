#ifndef DIALOGFIND_H
#define DIALOGFIND_H

#include <QDialog>

namespace Ui {
class dialogFind;
}

/**
 * @brief Dialog find for fast text search
 */
class dialogFind : public QDialog
{
    Q_OBJECT

public:
    QString mSearchStr;

    explicit dialogFind(QWidget *parent = 0);
    ~dialogFind();

private slots:
    void on_btnFind_clicked();

    void on_btnCancel_clicked();

private:
    Ui::dialogFind *ui;
};

#endif // DIALOGFIND_H
