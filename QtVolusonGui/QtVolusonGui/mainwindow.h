#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define CMAKE_USED

#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QFileSystemModel>
#include <QTextCursor>
#include <QGraphicsColorizeEffect>
#include "cvolusoninterface.h"
#include "dcmqfilesystemmodel.h"
#include "loadimgsthread.h"
#include "dialogfind.h"

#ifdef CMAKE_USED
    #include "qvtkdicomwidget.h"
#endif

#define FRAMES_FOLDER_NAME ".frames"

// DICOM tags definitions:
#define DICOM_TAG_NumberOfFrames "0028,0008"
#define DICOM_TAG_LossyImageCompression "0028,2110"

/**
 * User interface namespace. Integrates MainWindow.
 */
namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    /** @brief Voluson interface class pointer */
    cVolusonInterface *mVolInterf;

    /** @brief Custom file model */
    dcmQFileSystemModel *mModel;

    /** @brief Graphics scene pointer */
    QGraphicsScene* mScene;

    /** @brief Currently selected index */
    QModelIndex mActIndex;

    /** @brief Server folder location */
    QString mServerPath;

    /** @brief Path of the selected dcm file */
    QString mSelectedPath;

    /** @brief Timer */
    QTimer *mTimerFrames;

    /** @brief Dialog find text */
    dialogFind *mFindDial;
    /** @brief Text cursor - remember current position */
    QTextCursor mTextCursor;

    /** @brief Cfg file loaded flag */
    bool mbCfgLoaded;

    /** @brief Server started flag */
    bool mbServerStarded;

    /** @brief Error indicator - false means *.cfg file is not selected */
    bool mbError;

    /** @brief Multiframe image selected indicator */
    bool mbMultiframeSelected;

    /** @brief Thread for loading multiframe images */
    loadImgsThread *mLoadImgThread;

    /** @brief Animation of the push button */
    QPropertyAnimation *mPAnimation;

    /** @brief Color efect var */
    QGraphicsColorizeEffect *aEffect;

    void closeEvent(QCloseEvent *event);

    void selectServerPath();

    void showDcmInfo(const QString aDcmPath);

    void showDCMImageQT(QString aDcmPath);
    void showDCMImageVTK(QString aDcmPath);

    void showFrame(int aFrame);

    void loadFrames();

    void startTimer();

    void newLoadImgThread(unsigned int aFrames, QString aFramePath, QString aSelPath);

    void refreshSpliter();

    void findText();

private slots:
    void on_btnStartServer_clicked();

    void on_btnFind_clicked();

    void on_actionExit_triggered();

    void updateServerInfo();

    void on_actionOpen_DICOM_file_triggered();

    void on_treeView_clicked(const QModelIndex &index);

    void treeViewKeyPressed();

    void on_actionOpen_cfg_triggered();

    void on_splitter_splitterMoved(int pos, int index);

    void on_splitter_2_splitterMoved(int pos, int index);

    void on_btnDownload_clicked();

    void on_sbFrame_valueChanged(int arg1);

    void on_btnPlay_clicked();

    void timerFrameUpdate();

    void on_sldSpeed_valueChanged(int value);

    void on_sldSpeed_sliderReleased();

    void on_actionCreate_server_folder_triggered();

    void loadImgChanged(int aNum);
    void loadImgEnded();

    void on_actionFind_text_triggered();

    void on_actionQGraphicView_triggered();

    void on_actionVTK_triggered();

    void on_btnFind_2_clicked();
    void on_btnClear_clicked();
};

#endif // MAINWINDOW_H
