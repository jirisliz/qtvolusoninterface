

#ifndef CVOLUSONINTERFACE_H
#define CVOLUSONINTERFACE_H

#include <QtCore>
#include <QObject>

#define CVOL_ERR_TAG_NOT_PRESENT "Unrecognized tag name."

/**
 * @brief Class implements thread where DICOM server runs
 */
class Thread : public QThread
{
public:
    Thread();
    Thread(QString aPath);
    ~Thread();

    bool isServerStarted();
    void stop();

private:
    bool mbServerStarted;
    QMutex mMutex;
    QString mStrOut;
    // path
    QString dcmtkServerPath;

    QProcess *sh;

    void run();
};

/**
 * @brief The cVolusonInterface class includes DICOM server control and also methods for DICOM files processing.
 * DCMTK library terminal apps must be installed - sudo apt-get install dcmtk
 */
class cVolusonInterface
{
private:
    QString mServerBuf;
    Thread *mT;

    static QString runProcess(QString aCmd, QStringList aArgs);

public:
    cVolusonInterface(QString aPath);
    ~cVolusonInterface();

    void startServer();
    void stopServer();
    bool isServerStarted();

    QString findStudies();
    QString getDCMFileTags(QString aFile);

    static QString getDCMFileTag(QString aFile, QString aTag);
    static QString convertDcmToImg(QString aDcmPath, QString aFramesPath, int aFrame);
    static QString decompressJpegDcm(QString aDcmPath, QString aOutPath);

    bool createServerFolder(QString aPath);

};

#endif // CVOLUSONINTERFACE_H
