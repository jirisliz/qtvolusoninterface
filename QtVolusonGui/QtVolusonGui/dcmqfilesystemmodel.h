#ifndef DCMQFILESYSTEMMODEL_H
#define DCMQFILESYSTEMMODEL_H
#include <QVariant>
#include <QFileSystemModel>

/**
 * @brief Custom system model class
 */
class dcmQFileSystemModel : public QFileSystemModel
{

public:
    dcmQFileSystemModel();
    QVariant data( const QModelIndex& index, int role ) const;

signals:

public slots:
};

#endif // DCMQFILESYSTEMMODEL_H
