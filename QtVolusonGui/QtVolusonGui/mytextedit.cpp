#include "mytextedit.h"
#include "dialogfind.h"
#include <QApplication>

myTextEdit::myTextEdit(QWidget *parent) : QTextEdit(parent)
{

}

void myTextEdit::keyPressEvent(QKeyEvent *aKey)
{
    // Pass to the aKey original method
//    myTextEdit::keyPressEvent(aKey);

    if ( (aKey->key() == Qt::Key_F)  && QApplication::keyboardModifiers() && Qt::ControlModifier)
        emit findSignal();
}
