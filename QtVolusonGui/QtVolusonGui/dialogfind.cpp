#include "dialogfind.h"
#include "ui_dialogfind.h"

dialogFind::dialogFind(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogFind)
{
    ui->setupUi(this);
    this->setWindowTitle("Find");
}

dialogFind::~dialogFind()
{
    delete ui;
}

void dialogFind::on_btnFind_clicked()
{
    mSearchStr = ui->lineEdit->text();
    this->accept();
}

void dialogFind::on_btnCancel_clicked()
{

    this->reject();
}
