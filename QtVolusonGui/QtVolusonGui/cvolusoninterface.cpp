#include "cvolusoninterface.h"
#include <QDebug>

/**
 * @brief Runs command line process
 * @param aCmd name of the command
 * @param aArgs list of cmd arguments
 * @return output of the command line process
 */
QString cVolusonInterface::runProcess(QString aCmd, QStringList aArgs)
{
    QProcess sh;
    sh.setProcessChannelMode(QProcess::MergedChannels);
    sh.start(aCmd, aArgs);

    sh.waitForFinished();
    QString output(sh.readAllStandardOutput());

    sh.close();
    sh.terminate();

    return output;
}

/**
 * @brief Constructor
 * @param aPath Location of the stored dcm files
 */
cVolusonInterface::cVolusonInterface(QString aPath)
{
    mT = new Thread(aPath);
    mServerBuf.clear();
}

/**
 * @brief Destructor
 */
cVolusonInterface::~cVolusonInterface()
{
    mT->stop();
    delete mT;
}

/**
 * @brief Start DICOM server thread
 */
void cVolusonInterface::startServer()
{
    // Start server thread
    if(!isServerStarted())
        mT->start();
}

void cVolusonInterface::stopServer()
{
    if(isServerStarted())
    {
        mT->stop();
    }
}

/**
 * @brief Check if DICOM server is started
 * @return true/false if the server runs/stopped
 */
bool cVolusonInterface::isServerStarted()
{
    return mT->isServerStarted();
}

/**
 * @brief Send dicom command Find - lists all data in DICOM server
 * @return Text out. list
 */
QString cVolusonInterface::findStudies()
{
    QString prog = "findscu";
    QStringList args;

    // cmd args:
    args << "-v" << "-S" << "-aet" << "VE6" << "-aec" << "VE6" << "localhost"
         << "11112" << "-k" << "QueryRetrieveLevel=STUDY" << "-k" << "StudyDate"
         << "-k" << "StudyDescription" << "-k" << "StudyInstanceUID" /*<< "-d"*/;

    QString output = runProcess(prog, args);

    return output;
}

/**
 * @brief List all DICOM tags includet in the imput dcm file
 * @param aFile dcm file location
 * @return Text out. list of the DICOM tags
 */
QString cVolusonInterface::getDCMFileTags(QString aFile)
{
        QString prog = "dcmdump";
        QStringList args;
        // cmd args:
        args << aFile;

        QString output = runProcess(prog, args);

        return output;
}

/**
 * @brief Reads one DICOM tag
 * @param aFile dcm file location
 * @param aTag DICOM tag code
 * @return
 */
QString cVolusonInterface::getDCMFileTag(QString aFile, QString aTag)
{
    QString prog = "dcmdump";
    QStringList args;

    // cmd args:
    args << "+P" << aTag << aFile;

    QString msg = runProcess(prog, args);

    // Chceck error
    if(msg.contains("unrecognized tag name"))
        return CVOL_ERR_TAG_NOT_PRESENT;

    // Extract tag value
    QRegExp rx("\\[(.*)\\]");
    rx.indexIn(msg, 0);
    QString output = rx.cap(1);

    return output;
}

/**
 * @brief Converts DICOM file to image file
 * @param aDcmPath input dcm file location
 * @param aFramesPath output location of the image
 * @param aFrame number of the frame (0 if there is only one image in dcm file)
 * @return path to the converted image
 */
QString cVolusonInterface::convertDcmToImg(QString aDcmPath, QString aFramesPath, int aFrame)
{
    QString jpgPath = aFramesPath + "/image" + QString::number(aFrame);
    // Delete previous image if present
    QFile::remove(jpgPath);

    QString prog = "dcmj2pnm";
    QStringList args;

    // cmd args:
    args << "--write-raw-pnm" << "+F" << QString::number(aFrame) << aDcmPath << jpgPath;

    QString output = runProcess(prog, args);

    return jpgPath;
}

/**
 * @brief Decompress Jpeg in dcm file
 * @param aDcmPath input dcm file location
 * @param aOutPath output dcm file location
 * @return path to the converted dcm file
 */
QString cVolusonInterface::decompressJpegDcm(QString aDcmPath, QString aOutPath)
{
    QString decPath = aOutPath + "/dec.dcm";
    // Delete previous image if present
    QFile::remove(decPath);

    QString prog = "dcmdjpeg";
    QStringList args;

    // cmd args:
    args << aDcmPath << decPath;

    QString output = runProcess(prog, args);

    return decPath;
}

/**
 * @brief Creates server folder with configuration (.cfg) file
 * @param aPath location of the DICOM server
 * @return true if successful, false if not
 */
bool cVolusonInterface::createServerFolder(QString aPath)
{
    QString dcmqrscp = "NetworkTCPPort  = 11112\
            MaxPDUSize = 28672\n\
            MaxAssociations = 16\n\n\
            HostTable BEGIN\n\
            ve6 = (VE6, localhost, 1234)\n\
            akhLocal = ve6\n\
            HostTable END\n\n\
            VendorTable BEGIN\n\
            \"AKH local\"   = akhLocal\n\
            VendorTable END\n\n\
            AETable BEGIN\n\
            VE6" + aPath + " RW (500,2048mb) ANY\n\
            AETable END";

    // Create cfg file
    QFile cfgFile(aPath + "/dcmqrscp.cfg");
    if(cfgFile.open(QIODevice::ReadWrite))
    {
        QTextStream stream( &cfgFile );
        stream << dcmqrscp << endl;
    }
    else return false;

    // Create VE6 folder
    if(!(QDir().mkpath(aPath + "/VE6")))
    {
        return false;
    }

    return true;
}

// ------------------------------------------------------------------------------------------
// Thread
// ------------------------------------------------------------------------------------------
/**
 * @brief Server thread constructor (with hartcoded path - testing only, use Thread(QString aPath) instead)
 */
Thread::Thread()
{
    // TODO: Do NOT hardcode path!
    dcmtkServerPath = "/home/george/work/QTProjects/my/QtVolusonInterface/dcmtkServer/";
    mMutex.lock();
    mbServerStarted = false;
    mMutex.unlock();
}

/**
 * @brief Server thread constructor
 * @param aPath location of the DICOM server
 */
Thread::Thread(QString aPath)
{
    dcmtkServerPath = aPath;
    sh = Q_NULLPTR;
    mMutex.lock();
    mbServerStarted = false;
    mMutex.unlock();
}

/**
 * @brief Destructor
 */
Thread::~Thread()
{
//    sh->close();
//    sh->terminate();
    if(sh == Q_NULLPTR)
        sh->deleteLater();
}

/**
 * @brief Test if server is running
 * @return true/false
 */
bool Thread::isServerStarted()
{
    return mbServerStarted;
}

/**
 * @brief Stops the Thread
 */
void Thread::stop()
{
    this->terminate();
    this->wait();
    mbServerStarted = false;
}

/**
 * @brief Run thread method
 */
void Thread::run()
{
    // TODO: test process if it is stable
    sh = new QProcess();

    sh->setProcessChannelMode(QProcess::MergedChannels);

    QString strPath(dcmtkServerPath);
    QString prog = "dcmqrscp";
    QStringList args;
    QString strPathCfg = strPath + "/dcmqrscp.cfg";
    args << "-c" << strPathCfg << "--prefer-jpeg8" /*<< "--propose-lossless"*/;

    sh->start(prog, args);
    mMutex.lock();
    mbServerStarted = true;
    mMutex.unlock();

    while(!sh->waitForFinished())
    {
        QString output(sh->readAllStandardOutput());
        qDebug() << output;
    }

    sh->close();
    sh->terminate();
}

